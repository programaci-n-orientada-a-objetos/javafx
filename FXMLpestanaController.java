/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea_javafx.pkg1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Jose Murillo Sancho
 */
public class FXMLpestanaController implements Initializable {

    @FXML
    private Button button;
    @FXML
    private Button button1;
    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void mal(ActionEvent e) {
        label.setText("Que mal, que te mejores");
    }
    
    @FXML
    private void bien(ActionEvent e) {
        label.setText("Que bueno");
    }
    
}
